package de.larcado.sesam;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.RemoteInput;

import de.larcado.sesam.model.Domain;
import de.larcado.sesam.model.JsonStorage;
import de.larcado.sesam.model.Profil;

import static de.larcado.sesam.NotificationHandler.DOMAIN_ACTION;
import static de.larcado.sesam.NotificationHandler.KEY_REPLY;
import static de.larcado.sesam.NotificationHandler.MASTERPASSWD_ACTION;
import static de.larcado.sesam.NotificationHandler.MASTERPASSWORD;

public class NotificationReplyReceiver extends BroadcastReceiver {

    public static Intent getReplyMessageIntent(Context context, String action) {
        Intent intent = new Intent(context, NotificationReplyReceiver.class);
        intent.setAction(action);
        return intent;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (MASTERPASSWD_ACTION.equals(intent.getAction())) {
            CharSequence replyMessage = getReplyMessage(intent);
            if (replyMessage != null) {
                NotificationHandler.createInsertDomain(context, replyMessage.toString());
            }

        } else if (DOMAIN_ACTION.equals(intent.getAction())) {
            System.out.println(DOMAIN_ACTION);
            String masterpassword = intent.getStringExtra(MASTERPASSWORD);
            CharSequence replyMessage = getReplyMessage(intent);
            if (replyMessage != null) {
                JsonStorage jsonStorage = JsonStorage.load(context);
                String domain = replyMessage.toString();
                for (Domain selectedDomain : jsonStorage.getDomains()) {
                    if (selectedDomain.getName().equals(domain)) {
                        Profil selectedProfil = jsonStorage.getProfilById(selectedDomain.getProfilID());
                        String password = Sesam.getPasswordSesam(context, masterpassword, selectedDomain, selectedProfil);
                        NotificationHandler.setPasswordNotification(context, domain, password);
                        NotificationHandler.createInsertDomain(context, masterpassword);
                        return;
                    }
                }
                NotificationHandler.setPasswordNotification(context, domain + " does not exist", "");
                NotificationHandler.createInsertDomain(context, masterpassword);
            }
        }
    }

    private static CharSequence getReplyMessage(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput.getCharSequence(KEY_REPLY);
        }
        return null;
    }
}

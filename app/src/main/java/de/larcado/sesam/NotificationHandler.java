package de.larcado.sesam;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;

import static android.os.Build.VERSION_CODES.N;

public class NotificationHandler {

    public static final String MASTERPASSWD_ACTION = "de.larcado.sesam.MASTERPW";
    public static final String DOMAIN_ACTION = "de.larcado.sesam.DOMAIN";
    public static final String KEY_REPLY = "keyReply";
    public static final String MASTERPASSWORD = "masterpassword";
    private static final int NOTIFICATION_ID = 12345;

    public static void createInsertMasterPw(Context context) {
        createNotification(context, "master password", MASTERPASSWD_ACTION, null);
    }

    public static void createInsertDomain(Context context, String masterpassword) {
        createNotification(context, "domain", DOMAIN_ACTION, masterpassword);
    }

    private static void createNotification(Context context, String replyLabel, String action, String masterpassword) {
        NotificationChannel channel = new NotificationChannel(
                "quick_settings_channel",
                "Quick Settings Channel",
                NotificationManager.IMPORTANCE_HIGH
        );
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.createNotificationChannel(channel);

        // 1. Build label
        RemoteInput remoteInput = new RemoteInput.Builder(KEY_REPLY)
                .setLabel(replyLabel)
                .build();

        // 2. Build action
        NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(
                R.drawable.tile_icon, replyLabel, getReplyPendingIntent(context, action, masterpassword))
                .addRemoteInput(remoteInput)
                .setAllowGeneratedReplies(true)
                .build();

        // 3. Build notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "quick_settings_channel")
                .setSmallIcon(R.drawable.tile_icon)
                .setShowWhen(true)
                .addAction(replyAction);

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private static PendingIntent getReplyPendingIntent(Context context, String action, String masterpassword) {
        if (Build.VERSION.SDK_INT >= N) {
            Intent intent = NotificationReplyReceiver.getReplyMessageIntent(context, action);
            intent.putExtra(MASTERPASSWORD, masterpassword);
            return PendingIntent.getBroadcast(
                    context.getApplicationContext(),
                    100,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
        }
        return null;
    }

    public static void setPasswordNotification(Context context, String domain, String password) {
        Clipboard.save(context, password);

        Intent intent = new Intent(context, ClearClipboard.class);
        intent.setAction("de.larcado.sesam.noti");
        PendingIntent deleteIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationChannel channel = new NotificationChannel(
                "passwort_channel",
                "Passwort Channel",
                NotificationManager.IMPORTANCE_HIGH
        );
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "passwort_channel")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(domain)
                        .setContentText("Password in clipboard!")
                        .setDeleteIntent(deleteIntent);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.createNotificationChannel(channel);
        mNotificationManager.notify(0, mBuilder.build());
    }
}

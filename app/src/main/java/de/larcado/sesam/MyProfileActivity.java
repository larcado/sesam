package de.larcado.sesam;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.larcado.sesam.model.JsonStorage;
import de.larcado.sesam.model.Profil;
import de.larcado.sesam.utils.AppDialog;

public class MyProfileActivity extends AppCompatActivity {

    private static final Pattern MIN_EIN_ZEICHEN = Pattern.compile(".+");

    private Typeface myTypeface;
    private JsonStorage jsonStorage;
    private RecyclerView recyclerView;
    private ProfilAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);

        jsonStorage = JsonStorage.load(this);
        myTypeface = Typeface.createFromAsset(getAssets(), "NotoSerif-Italic.ttf");

        recyclerView = findViewById(R.id.recycler_view);

        mAdapter = new ProfilAdapter(this, jsonStorage.getProfile(),
                profil -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppDialogTheme);
                    builder.setTitle("Edit entry");
                    LayoutInflater inflater = this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_profile, null);
                    EditText e_name = dialogView.findViewById(R.id.name);
                    e_name.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            Matcher matcher = MIN_EIN_ZEICHEN.matcher(s.toString());
                            if (!matcher.matches()) {
                                e_name.setError("This entry must conatin at least one character");
                            } else {
                                e_name.setError(null);
                            }
                        }
                    });
                    e_name.setText(profil.getName());
                    EditText e_length = dialogView.findViewById(R.id.length);
                    e_length.setText("" + profil.getLength());
                    EditText e_sonderzeichen = dialogView.findViewById(R.id.sonderzeichen);
                    e_sonderzeichen.setTypeface(myTypeface);
                    e_sonderzeichen.setText(profil.getSonderzeichen());
                    builder.setView(dialogView);
                    builder.setPositiveButton("OK", (dialog, which) -> {
                        profil.setName(e_name.getText().toString());
                        profil.setLength(Integer.parseInt(e_length.getText().toString()));
                        profil.setSonderzeichen(e_sonderzeichen.getText().toString());
                        jsonStorage.save(this);
                        Toast.makeText(this, profil.getName() + " changed", Toast.LENGTH_LONG).show();
                        refresh();
                        dialog.dismiss();
                    });
                    builder.show();
                }, profil -> {
            AppDialog.yesNoAlertDialog(this,
                    "Delete?",
                    "Do you want to delete this entry?",
                    (dialog, which) -> {
                        Toast.makeText(this, profil.getName() + " deleted", Toast.LENGTH_LONG).show();
                        refresh();
                    }, null
            );
        });
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        refresh();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Settings settings = new Settings(this);

            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppDialogTheme);
            builder.setTitle("New entry");
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_profile, null);
            EditText e_name = dialogView.findViewById(R.id.name);
            e_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    Matcher matcher = MIN_EIN_ZEICHEN.matcher(s.toString());
                    if (!matcher.matches()) {
                        e_name.setError("This entry must conatin at least one character");
                    } else {
                        e_name.setError(null);
                    }
                }
            });
            EditText e_length = dialogView.findViewById(R.id.length);
            e_length.setText("" + settings.getLengthOfPasswords());
            EditText e_sonderzeichen = dialogView.findViewById(R.id.sonderzeichen);
            e_sonderzeichen.setText(settings.getSpecialCharacters());
            e_sonderzeichen.setTypeface(myTypeface);
            builder.setView(dialogView);
            builder.setPositiveButton("OK", (dialog, which) -> {
                int nextID = 0;
                for (Profil p : jsonStorage.getProfile()) {
                    if (p.getId() >= nextID) {
                        nextID = p.getId() + 1;
                    }
                }
                Profil profil = new Profil();
                profil.setId(nextID);
                profil.setName(e_name.getText().toString());
                profil.setLength(Integer.parseInt(e_length.getText().toString()));
                profil.setSonderzeichen(e_sonderzeichen.getText().toString());
                jsonStorage.getProfile().add(profil);
                jsonStorage.save(this);
                Toast.makeText(this, profil.getName() + " added", Toast.LENGTH_LONG).show();
                refresh();
                dialog.dismiss();
            });
            builder.show();
        });
    }

    private void refresh() {
        mAdapter.notifyDataSetChanged();
    }
}

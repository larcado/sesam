package de.larcado.sesam;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;

import de.larcado.sesam.model.Domain;
import de.larcado.sesam.model.JsonStorage;
import de.larcado.sesam.model.Profil;
import de.larcado.sesam.utils.AppDialog;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private JsonStorage jsonStorage;
    private Spinner domainSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestStoragePermission();

        jsonStorage = JsonStorage.load(this);

        final EditText masterpwd = findViewById(R.id.masterpwd);
        domainSpinner = findViewById(R.id.domain);
        refreshSpinnerItems();

        final Button button = findViewById(R.id.button);
        final TextView pwd = findViewById(R.id.pwd);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "NotoSerif-Italic.ttf");
        pwd.setTypeface(myTypeface);

        final ProgressBar progress = findViewById(R.id.progress);
        final TextView details = findViewById(R.id.detail);

        button.setOnClickListener(v -> {
            final Domain selectedDomain = (Domain) domainSpinner.getSelectedItem();
            final Profil selectedProfil = jsonStorage.getProfilById(selectedDomain.getProfilID());
            final String s_masterpwd = masterpwd.getText().toString();

            details.setText("");
            pwd.setText("please wait...");

            if (!s_masterpwd.equals("")) {
                button.setEnabled(false);
                selectedDomain.setUsecount(selectedDomain.getUsecount() + 1);
                Collections.sort(jsonStorage.getDomains());
                jsonStorage.save(this);
                refreshSpinnerItems(selectedDomain);
                Observable.create((ObservableOnSubscribe<String>) e -> {
                    e.onNext(Sesam.getPasswordSesam(MainActivity.this, s_masterpwd, selectedDomain, selectedProfil, progress, details));
                    e.onComplete();
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(s -> {
                            pwd.setText(s);
                            button.setEnabled(true);
                        });
            } else {
                Toast.makeText(MainActivity.this, "You must specify a master password and a domain", Toast.LENGTH_SHORT).show();
            }
        });

        pwd.setOnClickListener(v -> {
            final Domain selectedDomain = (Domain) domainSpinner.getSelectedItem();
            final String s_domain = selectedDomain.getName();
            NotificationHandler.setPasswordNotification(this, s_domain, pwd.getText().toString());
        });
    }

    private void refreshSpinnerItems() {
        refreshSpinnerItems(null);
    }

    private void refreshSpinnerItems(Domain selectedDomain) {
        jsonStorage = JsonStorage.load(this);
        ArrayAdapter<Domain> spinnerAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                jsonStorage.getDomains()
        );
        domainSpinner.setAdapter(spinnerAdapter);
        if (selectedDomain != null) {
            for (int i = 0; i < jsonStorage.getDomains().size(); i++) {
                Domain d = jsonStorage.getDomains().get(i);
                if (d.getName().equals(selectedDomain.getName())) {
                    domainSpinner.setSelection(i, false);
                    return;
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshSpinnerItems();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_liste) {
            Intent intent = new Intent(this, MyDomainsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                AppDialog.yesNoAlertDialog(
                        this,
                        "Permission WRITE_EXTERNAL_STORAGE required",
                        "In order to save your settings in a local json file (~/Sesam/sesam.json), this app needs this permission.",
                        (dialog, w) -> {
                            dialog.dismiss();
                            Toast.makeText(this, "Great :D", Toast.LENGTH_LONG).show();
                            doRequstPermission();
                        },
                        (dialog, w) -> {
                            dialog.dismiss();
                            Toast.makeText(this, "Ok, but you can't use this app then.", Toast.LENGTH_LONG).show();
                            finish();
                        }
                );
            } else {
                // No explanation needed; request the permission
                doRequstPermission();
            }
        } else {
            // Permission has already been granted
        }
    }

    private void doRequstPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
    }
}

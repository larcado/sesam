package de.larcado.sesam;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ClearClipboard extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Clipboard.save(context, "");
        Toast.makeText(context, "Password removed from clipboard!", Toast.LENGTH_LONG).show();
    }
}

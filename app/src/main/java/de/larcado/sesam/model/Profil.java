package de.larcado.sesam.model;

public class Profil {
    private int id;
    private String name;
    private int length;
    private String sonderzeichen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getSonderzeichen() {
        return sonderzeichen;
    }

    public void setSonderzeichen(String sonderzeichen) {
        this.sonderzeichen = sonderzeichen;
    }

    @Override
    public String toString() {
        return name + "{" + length +
                ", " + sonderzeichen + " " +
                '}';
    }
}

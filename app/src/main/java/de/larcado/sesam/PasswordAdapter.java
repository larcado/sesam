package de.larcado.sesam;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.larcado.sesam.model.Domain;
import de.larcado.sesam.model.JsonStorage;
import de.larcado.sesam.utils.Logger;
import io.reactivex.functions.Consumer;

public class PasswordAdapter extends RecyclerView.Adapter<PasswordAdapter.MyViewHolder> {

    private JsonStorage jsonStorage;
    private List<Domain> domainList;
    private Consumer<Domain> onClick;
    private Consumer<Domain> onLongClick;
    private Typeface myTypeface;

    public PasswordAdapter(Context context, JsonStorage jsonStorage, List<Domain> domainList, Consumer<Domain> onClick, Consumer<Domain> onLongClick) {
        this.jsonStorage = jsonStorage;
        this.domainList = domainList;
        this.onClick = onClick;
        this.onLongClick = onLongClick;

        myTypeface = Typeface.createFromAsset(context.getAssets(), "NotoSerif-Italic.ttf");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Domain domain = domainList.get(position);
        holder.domain.setText(domain.getName());
        holder.profil.setTypeface(myTypeface);
        holder.profil.setText(jsonStorage.getProfilById(domain.getProfilID()).toString());

        holder.root.setOnClickListener(v -> {
            try {
                onClick.accept(domain);
            } catch (Exception e) {
                Logger.e(this.getClass(), e);
            }
        });

        holder.root.setOnLongClickListener(v -> {
            try {
                onLongClick.accept(domain);
                return true;
            } catch (Exception e) {
                Logger.e(this.getClass(), e);
            }
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return domainList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public View root;
        public TextView domain, profil;

        public MyViewHolder(View view) {
            super(view);
            root = view;
            domain = view.findViewById(R.id.domain);
            profil = view.findViewById(R.id.profil);
        }
    }
}